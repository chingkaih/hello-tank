/*
 * Copyright (c) 2015 Wentworth Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var http = require("http");
var url = require('url');
var fs = require('fs');
var io = require('socket.io');

var tankNum = -1;
var tankMaxNum = 4;
var isTank = false;

var server = http.createServer(function(request, response) {
	//console.log('HTTP: Create Server');
	var path = url.parse(request.url).pathname;

	//console.log(path);

	switch (path) {
		case '/':
			tankNum++;
			if(tankNum < tankMaxNum){
				isTank = true;
				fs.readFile(__dirname + "/tank2.html", function(error, data) {
					if (error){
						response.writeHead(404);
						response.write("opps this doesn't exist - 404");
						return;
					} else {
						response.writeHead(200, {"Content-Type": "text/html"});
						response.write(data, "utf8");
					}
					response.end();
				});
				console.log("### Add Tank" + tankNum);
			}else{
				isTank = false;
				fs.readFile(__dirname + "/viewer.html", function(error, data) {
					if (error){
						response.writeHead(404);
						response.write("opps this doesn't exist - 404");
						return;
					} else {
						response.writeHead(200, {"Content-Type": "text/html"});
						response.write(data, "utf8");
					}
					response.end();
				});
				console.log("### Add Viewer");
			}
			break;
		case '/js/three.min.js':
		case '/js/Detector.js':
		case '/js/accelerometer.js':
		case '/js/gyroscope.js':
		case '/3D_module/tank0.js':
		case '/3D_module/tank1.js':
		case '/3D_module/tank2.js':
		case '/3D_module/tank3.js':
		case '/3D_module/field.js':
		case '/js/battle_field.js':
			fs.readFile(__dirname + path, function(error, data) {
				if (error){
					response.writeHead(404);
					response.write("opps this doesn't exist - 404");
					return;
				} else {
					response.writeHead(200, {"Content-Type": "application/javascript"});
					response.write(data, "utf8");
				}
				response.end();
			});
			break;
		case '/images/checkerboard.jpg':
			fs.readFile(__dirname + path, function(error, data) {
				if (error){
					response.writeHead(404);
					response.write("opps this doesn't exist - 404");
					return;
				} else {
					response.writeHead(200, {"Content-Type": "image/jpeg"});
					response.write(data, "utf8");
				}
				response.end();
			});
			break;
		case '/viewer.html':
		case '/tank.html':
		case '/tank2.html':
		case '/tankR.html':
		case '/test.html':	
		case '/alpha.html':
		case '/client.html':
			isTank = false;
			fs.readFile(__dirname + path, function(error, data) {
				if (error){
					response.writeHead(404);
					response.write("opps this doesn't exist - 404");
					return;
				} else {
					response.writeHead(200, {"Content-Type": "text/html"});
					response.write(data, "utf8");
				}
				response.end();
			});
			break;
		default:
			response.writeHead(404);
			response.write("opps this doesn't exist - 404");
			response.end();
			return;
			break;
	}
});

server.listen(process.env.PORT || 8001); 

var tank_defPositions = [];
tank_defPositions[0] = [ 200, 200];
tank_defPositions[1] = [-200, 200];
tank_defPositions[2] = [-200,-200];
tank_defPositions[3] = [ 200,-200];

// serialNum, position x, y, z, rotation beta, gamma, alpha
var tanks = [];
tanks[0] = [-1,0,0,0,-1,-1,-1];
tanks[1] = [-1,0,0,0,-1,-1,-1];
tanks[2] = [-1,0,0,0,-1,-1,-1];
tanks[3] = [-1,0,0,0,-1,-1,-1];

var SN = 0;
var serv_io = io.listen(server);
var nodeList = [];

serv_io.sockets.on('connection', function(socket) {
	console.log("Event: connection");

	SN++;
	var thisSN = SN;
	var timeout = 1000;

	if(isTank){
		console.log("IS TANK" + tankNum);
		for(var x=0; x<4; x++){
			if(tanks[x][0] == -1){
				tanks[x][0] = thisSN;
				printLog(thisSN, "Send: new_tank" + x);
				socket.emit('new_tank', { 'num': x, 'sn':thisSN, 'x':tank_defPositions[tankNum][0], 'z':tank_defPositions[tankNum][1]});
				break;
			}
		}
	}else{
		console.log("NOT TANK" + tankNum);
	}

	socket.on('update_location', function(data){
		var index = tankIndex(data.sn);
		tanks[index] = [data.sn, data.lx, data.ly, data.lz, data.rx, data.ry, data.rz];
		timeout = 1000;
	});

	socket.on('update', function(){
		timeout = 1000;


	});

	socket.on('cannon_upload', function(data){
		socket.broadcast.emit('renew_cannon', data);
	});

	var checkTimeout = setInterval(function(){
		timeout--;
		//printLog(thisSN, ">>> Timeout = " + timeout);

		if(timeout < 500){
			if(isTank && tankIndex(thisSN) != -1){
				printLog(thisSN, "Rmove Tank" + tankIndex(thisSN));
				//printLog(thisSN, "Timeout = " + timeout);
				tanks[tankIndex(thisSN)][0] = -1;
				tankNum--;
			}

			socket.emit('update_tanks', tanks);
			clearInterval(checkTimeout);
			socket.leave();
		}else{
			socket.emit('update_tanks', tanks);
		}
	},10);
});

function tankIndex(snum){
	for(var x=0; x<4; x++){
		if(tanks[x][0] == snum)
			return x;
	}
	return -1;
}

function printLog(node,msg){
	console.log("[" + node + "]: " + msg);
}


