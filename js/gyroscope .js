window.addEventListener('deviceorientation', oriChange, false);
var gyro_x = 0, gyro_y = 0, gyro_z = 0;

function oriChange(e){
	gyro_x = formatFloat(e.beta,3);
	gyro_y = formatFloat(e.gamma,3);
	gyro_z = formatFloat(e.alpha,3);
}

function gyroX(){
	return gyro_x;
}
function gyroY(){
	return gyro_y;
}
function gyroZ(){
	return gyro_z;
}

