var acc_x = 0, acc_y = 0, acc = 0;

if (window.DeviceMotionEvent != undefined){
	window.ondevicemotion = function(e){
		acc_x = formatFloat(e.accelerationIncludingGravity.x, 3);
		acc_y = formatFloat(e.accelerationIncludingGravity.y, 3);
		acc_z = formatFloat(e.accelerationIncludingGravity.z, 3);
	}
}

function accX(){
	return acc_x;
}
function accY(){
	return acc_y;
}
function accZ(){
	return acc_z;
}

